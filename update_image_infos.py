import os
import cv2
import glob
import pickle

def get_image_name(img1):
    image_name1 = (os.path.basename(img1))
    return image_name1

project_files = (glob.glob(os.path.join('//10.10.5.98/merge_dataset/train', '*.jpg')))+ (glob.glob(os.path.join('//10.10.5.98/merge_dataset/train', '*.jpeg')))
old_dic = {}
name_list=[]
shape_list=[]

for index, img_path in enumerate(project_files):
    img_name=get_image_name(img_path)
    name_list.append(img_name)
    img=cv2.imread(img_path)
    shape= img.shape
    shape_list.append(shape)

old_dic=dict(zip(name_list, shape_list))
with open('saved_dictionary.pkl', 'wb') as f:
    pickle.dump(old_dic, f)