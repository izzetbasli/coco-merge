import os
from addict import Dict
import json
import pickle

def config_reader(path: str) -> Dict:
    with open(path) as f:
        cfg = json.load(f)
    return Dict(cfg)

def read_coco(path,filename):
    '''
    :param path: path of your json file
    :param filename: name of json file with '.json' extension.(example='train.json')
    :return:
    '''
    basePath = os.path.join(path)
    coco = config_reader(os.path.join(basePath, filename))
    return coco

def save_coco_file(coco,filename='sample'):
    with open(filename+'.json', "w") as fp:
        json.dump(coco, fp)

with open('saved_dictionary.pkl', 'rb') as f:
    loaded_dict = pickle.load(f)
coco=read_coco('//10.10.5.98/merge_dataset','merged_train_fix_3.json')

for img in coco['images']:
    h,w,c=loaded_dict[img['file_name']]
    img['width'] = w
    img['height'] = h

save_coco_file(coco,'merged_train_fix_4.json')