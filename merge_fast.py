import os
from addict import Dict
import json
import numpy as np
import pickle
from tqdm import tqdm

def config_reader(path: str) -> Dict:
    with open(path) as f:
        cfg = json.load(f)
    return Dict(cfg)

def read_coco(path,filename):
    '''
    :param path: path of your json file
    :param filename: name of json file with '.json' extension.(example='train.json')
    :return:
    '''
    basePath = os.path.join(path)
    coco = config_reader(os.path.join(basePath, filename))
    return coco

def save_coco_file(coco,filename='merged_train_fix_2'):
    with open(filename+'.json', "w") as fp:
        json.dump(coco, fp)

coco1=read_coco('//10.10.5.98/merge_dataset','instances_shape_training2021_v6.json')
coco2=read_coco('//10.10.5.98/merge_dataset','train_for_new_2.json')


cat1=coco1['categories']
cat2=coco2['categories']
cat3=cat1 + cat2

img1=coco1['images']
img2=coco2['images']
img3=img1 + img2

ann1=coco1['annotations']
ann2=coco2['annotations']
ann3=ann1+ann2

name_list=['categories','images','annotations']
value_list= [cat3,img3,ann3]
new_dict=dict(zip(name_list, value_list))

save_coco_file(new_dict)